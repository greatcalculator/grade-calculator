$(document).ready( function () {
	console.log("Hi");
    let current_table_row = 1;

    $("#new-assignment").click( function(event) {
        current_table_row++;

        $("#grade-table").find("tbody").append($("<tr class='assignment-row'>")
            .append($("<td>")
            .append($('<input type="text" class="assignment-name" id="assignment-name-${current_table_row}">')))
            .append($("<td>")
            .append($('<input type="text" class="assignment-weight" id="assignment-weight-${current_table_row}">')))
            .append($("<td>")
            .append($('<input type="text" class="assignment-grade" id="assignment-grade-${current_table_row}">'))));
    });

	// Calculate the current grade for the class
    let recalculate = function () {
        let total_grade = 0;
		let total_weight = 0;
        $(".assignment-row").each(function () {
            // console.log($(this).find(".assignment-grade").val());
            // console.log(parseFloat($(this).find(".assignment-weight").val()));
			
			let additional_weight = parseFloat($(this).find(".assignment-weight").val());
            let additional_grade = parseFloat($(this).find(".assignment-grade").val()) *
                additional_weight/100;

            if(!isNaN(additional_grade)) {
                total_grade += additional_grade;
				total_weight += additional_weight;
            }

        });
		console.log(total_weight);
        return total_grade/total_weight;
    };

    $("#recalculate-button").click(function () {
        $("#current-grade").text(recalculate());
    });

    $("#grade-entry").on('keyup change paste', 'input, select, textarea', function () {
        $("#current-grade").text(recalculate());
    });


});
