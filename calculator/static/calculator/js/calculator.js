$(document).ready( function () {

    let current_table_row = 1;

    $("#new-assignment").click( function(event) {
        current_table_row++;

        $("#grade-table").find("tbody").append($("<tr class='assignment-row'>")
            .append($("<td>")
            .append($('<input type="text" class="assignment-name" id="assignment-name-${current_table_row}">')))
            .append($("<td>")
            .append($('<input type="text" class="assignment-weight" id="assignment-weight-${current_table_row}">')))
            .append($("<td>")
            .append($('<input type="text" class="assignment-grade" id="assignment-grade-${current_table_row}">'))));
    });

	// Calculate the current grade for the class
    let recalculate_current = function () {
		let current_grade = 0
		let total_grade = 0;
		let total_weight = 0;
		
        $(".assignment-row").each(function () {
            // console.log($(this).find(".assignment-grade").val());
            // console.log(parseFloat($(this).find(".assignment-weight").val()));
			
			let additional_weight = parseFloat($(this).find(".assignment-weight").val());
            let additional_grade = parseFloat($(this).find(".assignment-grade").val()) *
                additional_weight/100;

            if(!isNaN(additional_grade)) {
                total_grade += additional_grade;
				total_weight += additional_weight;
            }

        });
		
		if(total_weight!=0) {
			// Set current grade, rounded to the nearest tenth
			current_grade = Math.round(total_grade/total_weight*1000)/10
		} else {
			current_grade = 0
		}
		
		return [current_grade,total_weight];
    };
	
	// Calculate the remaining grade needed for the entered ranges
	let grade_range_text = function (target_grade,current_grade,total_weight) {
		let remaining_grade = 0;
		let points_needed = target_grade - current_grade*total_weight/100;
		let remaining_points = 100 - total_weight;
		
		if(remaining_points==0) {
			if(points_needed > 0) {
				return "It is not possible to get a " + target_grade;
			}
		} else {
			remaining_grade = Math.round(points_needed/remaining_points*1000)/10;
		} 
		
		return "You need an average of " + remaining_grade + " going forward to get a " + target_grade;
	}
	
	// Set all grade values on calculator page (current and ranges)
	let recalculate_all = function() {
		let grade_values = recalculate_current();
		let current_grade = grade_values[0]
		let total_weight = grade_values[1]
		
		$("#current-grade").text(current_grade);
		
		// Set display needed for grade range
		let grade_ar = [90,80,70];
		var i;
		for(i in grade_ar) {
			$("#grade_range_"+grade_ar[i]).text(grade_range_text(grade_ar[i],current_grade,total_weight));
		}		
	}

    $("#recalculate-button").click(function () {
        recalculate_all();
    });

    $("#grade-entry").on('keyup change paste', 'input, select, textarea', function () {
		recalculate_all();
    });


});
